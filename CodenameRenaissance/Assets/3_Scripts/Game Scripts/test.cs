﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour {

    private float turn = 0;
    private bool changing = false;
    bool startFunction = false;

    IEnumerator ChangeLane(Transform car, float angle, float time)
    {
        float t;
        float bank;
        if (!changing)
        {
            changing = true;

            for (t = 0; t < 1;)
            {
                t += 2 * Time.deltaTime / time;
                turn = Mathf.Lerp(turn, angle, t);
                bank = 0/*.5f*/ * turn;
                car.localEulerAngles = new Vector3(0, turn, bank);

                yield return null;
            }

            for (t = 0; t < 1;)
            {
                t += 2 * Time.deltaTime / time;
                turn = Mathf.Lerp(turn, 0, t);
                bank = 0/*.5f*/ * turn;
                car.localEulerAngles = new Vector3(0, turn, bank);
                yield return null;
            }

            changing = false;
        }
    }

    // This is just for testing purposes - you must call ChangeLane in your script // when changing lanes. Attach this script to the car to test it.

    void Update()
    {
        if (Input.GetKeyDown("a"))
        {
            //StartCoroutine(ChangeLane(transform, -5, 0.6f));
            startFunction = true;
            rotateCar(transform, -5, 0.6f);
        }

        if (Input.GetKeyDown("d"))
        {
            //StartCoroutine(ChangeLane(transform, 5, 0.6f));
            startFunction = true;
            rotateCar(transform, 5, 0.6f);
        }
    }

    void rotateCar(Transform car, float angle, float time)
    {
        if (startFunction)
        {
            Debug.Log("Start Function");
            float t = 0;
            float bank;

            if (!changing)
            {
                changing = true;

                for (t = 0; t < 1;)
                {
                    t += 2 * Time.deltaTime / time;
                    turn = Mathf.Lerp(turn, 0, t);
                    bank = 0/*.5f*/ * turn;
                    car.localEulerAngles = new Vector3(0, turn, bank);
                    //break;
                }

                for (t = 0; t < 1;)
                {
                    t += 2 * Time.deltaTime / time;
                    turn = Mathf.Lerp(turn, 0, t);
                    bank = 0/*.5f*/ * turn;
                    car.localEulerAngles = new Vector3(0, turn, bank);
                    //break;
                    //yield return null;
                }

                changing = false;
                startFunction = false;
            }
        }
    }
}
