﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Lane
{
    LeftLane,
    RightLane
}

public enum CurrentLane
{
    LeftLane,
    RightLane
}

public class ControlScript : MonoBehaviour {

    [Header("Cameras")]
    public Camera mainCamera;

    [Header("Car Variables")]
    public GameObject car;
    public Transform leftLane;
    public Transform rightLane;
    public Lane headingTowards = Lane.LeftLane;
    public float sideSpeed = 4f;

    private RaycastHit hit;
    LayerMask targetLayer;

    [Header("New test variables")]
    public float rotateSpeed = 2f;
    public bool rotating = false;
    float rotationTime = 0;
    Vector3 relativePosition;
    Quaternion targetRotation;
    Transform target;

    void Awake () {
        // create a layer to target. i.e. ignore all other layers except Default layer. Used for the Raycast
        targetLayer = 1 << LayerMask.NameToLayer("Default");
    }
	
	
	void Update () {

        #if UNITY_EDITOR
        // mouse input logic for testing purposes
        mouseInput();
        #endif

        #if UNITY_ANDROID
        touchInput();
        #endif

        changeLanes();
    }

    void touchInput()
    {
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                Ray ray = mainCamera.ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit, 100, targetLayer) && touch.phase == TouchPhase.Began)
                {
                    if (hit.collider.tag == "mainGameInput")
                    {
                        //getDirection();
                    }

                    if (hit.collider.tag == "minigameInput")
                    {
                        //minigame_getDirection();
                    }
                }
            }
        }
    }

    void mouseInput()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100, targetLayer))
            {
                if (hit.collider.tag == "mainGameInput")
                {
                    determineLaneSwitch();
                }

                if (hit.collider.tag == "phoneInput")
                {
                    Debug.Log("Mouse Phone Input");
                    //minigame_getDirection();
                }
            }
        }

    }

    void determineLaneSwitch()
    {
        if(headingTowards == Lane.LeftLane)
        {
            headingTowards = Lane.RightLane;

            // rotating stuff test
            //target = rightLane;
        }
        else
        {
            headingTowards = Lane.LeftLane;

            // rotating stuff test
            //target = leftLane;
        }
        // rotating stuff test
        //relativePosition = target.position - car.transform.position;
        //targetRotation = Quaternion.LookRotation(relativePosition);
        //rotating = true;
        //rotationTime = 0;
    }

    void changeLanes()
    {
        // Car is heading towards the RIGHT LANE, so switch to RIGHT LANE
        if (headingTowards == Lane.RightLane)
        {
            // Smoothly switch lanes to RIGHT LANE
            car.transform.position = Vector3.Lerp(car.transform.position, rightLane.position, Time.deltaTime * sideSpeed);

            
        }
        // Car is heading towards the LEFT LANE, so switch to LEFT LANE
        else if (headingTowards == Lane.LeftLane)
        {
            // Smoothly switch lanes to LEFT LANE
            car.transform.position = Vector3.Lerp(car.transform.position, leftLane.position, Time.deltaTime * sideSpeed);
        }

        // rotating stuff test
        //if (rotating)
        //{
        //    rotationTime += Time.deltaTime * rotateSpeed;
        //    car.transform.rotation = Quaternion.Lerp(car.transform.rotation, targetRotation, rotationTime);


        //    if (rotationTime > 0.1f)
        //    {
        //        rotating = false;
        //    }
        //}
    }
}
