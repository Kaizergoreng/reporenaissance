== Main Game Details ==

Left lane: -0.5f
Right lane: 0.5f

Dashboard model scale: 0.5
=======================

== Mini Game Details ==

Left lane: 0.019f
Right lane: -0.014f
=======================

--------- 14/11/2018 ---------

## Set up minigame camera ##

## Set up layer masks ##

--------- 15/11/2018 ---------

## Add basic controls (Made input detection for mobile & pc) ##

## Added basic lane switching mechanism ##

--------- 24/11/2018 ---------

## Make smooth lane switching movement ##

## Make lane switching to be interruptible (boleh tukar lane while switching lanes) ##

** Continue rotating code.[Reference: https://www.youtube.com/watch?v=KJ7yT7KGuMM] **

** Refine lane switching mechanism. Make it to have some swerve **

** Add obstacles mover **